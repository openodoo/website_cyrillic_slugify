# -*- coding: utf-8 -*-
{
    'name': "website_cyrillic_slugify",
        
    'summary': """
        Update base slug for use with cyrillic name.
    """,

    'description': """
        Update base slug for use with cyrillic name.
    """,

    'author': "Open Odoo",
    'website': "https://open-odoo.ru",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Localization',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['website'],
    'external_dependencies': {
        # 'python' : ['python-slugify'],
    },

    # always loaded
    'data': [],
    # only loaded in demonstration mode
    'demo': [],
}